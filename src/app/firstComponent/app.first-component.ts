import {Component} from '@angular/core';

@Component({
    selector: 'app-firstComponent',
    templateUrl: './app.first-component.html'
  })
export class appFirstComponent{
    version:number = 4;
    course = 'Angular';
    input = true;

    inputvalue = 'Anu';

    constructor(){
        this.input = false;
    }

    returnCourse(){
        return this.course;
    }
    
    catchInput(event){
        console.log(event.value);
        this.inputvalue = event.target.value;
    }


}